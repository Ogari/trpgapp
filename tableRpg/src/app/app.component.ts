import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Home } from '../pages/home/home';
import { Dice } from "../pages/dice/dice";
import { Monsters } from "../pages/monsters/monsters";
import { MonsterInfo } from "../pages/monster-info/monster-info";
import { SpellOptions } from "../pages/spell-options/spell-options";
import { Spells } from "../pages/spells/spells";
import { SpellInfo } from "../pages/spell-info/spell-info";
import { Shop } from "../pages/shop/shop";
import { PlayerGuide } from "../pages/player-guide/player-guide";
import { Class } from "../pages/class/class";
import { ClassLevelInfo } from "../pages/class-level-info/class-level-info";
import { Race } from "../pages/race/race";

//import { HTTP } from '@angular/http';


@Component({
  templateUrl: 'app.html',
  // providers:[

  // ]
})
export class MyApp {
  rootPage:any = Home;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
