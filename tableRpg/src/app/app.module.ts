import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

// All pages imported
import { Home } from '../pages/home/home';
import { Dice } from "../pages/dice/dice";
import { Monsters } from "../pages/monsters/monsters";
import { MonsterInfo } from "../pages/monster-info/monster-info";
import { SpellOptions } from "../pages/spell-options/spell-options";
import { Spells } from "../pages/spells/spells";
import { SpellInfo } from "../pages/spell-info/spell-info";
import { Shop } from "../pages/shop/shop";
import { PlayerGuide } from "../pages/player-guide/player-guide";
import { Class } from "../pages/class/class";
import { ClassLevelInfo } from "../pages/class-level-info/class-level-info";
import { Race } from "../pages/race/race";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpModule } from '@angular/http';
import { RpgDatabaseApi } from '../shared/rpg-database-api-service';

@NgModule({
  declarations: [
    MyApp,
    Home,
    Dice,
    Monsters,
    MonsterInfo,
    SpellOptions,
    Spells,
    SpellInfo,
    Shop,
    PlayerGuide,
    Class,
    ClassLevelInfo,
    Race
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Home,
    Dice,
    Monsters,
    MonsterInfo,
    SpellOptions,
    Spells,
    SpellInfo,
    Shop,
    PlayerGuide,
    Class,
    ClassLevelInfo,
    Race
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    RpgDatabaseApi,
    HttpModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
