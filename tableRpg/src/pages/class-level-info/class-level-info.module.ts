import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ClassLevelInfo } from './class-level-info';

@NgModule({
  declarations: [
    ClassLevelInfo,
  ],
  imports: [
    // IonicModule.forChild(ClassLevelInfo),
  ],
  exports: [
    ClassLevelInfo
  ]
})
export class ClassLevelInfoModule {}
