import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-class-level-info',
  templateUrl: 'class-level-info.html',
})
export class ClassLevelInfo {

  allInfo = {};

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClassLevelInfo');
    this.allInfo = this.navParams.data;
    // this.initialize();
  }

  initialize(){
    // Primeiro capto o objeto aind wrapped e corrijo sua formatacao de texto, passando-o para um vetor
    var wrapped : any;
    wrapped = this.navParams.data;
    this.navParams.data.info = wrapped.info.split("|");

    // Repasso agora os dados para um objeto formal (definido)
    this.allInfo = this.navParams.data;
  }

// Used to separate in bold and regular text
  textFormatting(text){
    if (text.indexOf('[') > -1){
      return true;
    } else{
      return false;
    }
  }

}
