import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Class } from './class';

@NgModule({
  declarations: [
    Class,
  ],
  imports: [
    // IonicModule.forChild(Class),
  ],
  exports: [
    Class
  ]
})
export class ClassModule {}
