import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ClassLevelInfo } from "../class-level-info/class-level-info"

@IonicPage()
@Component({
  selector: 'page-class',
  templateUrl: 'class.html',
})
export class Class {

  unwrappedClassObj : any;
  //testingText : string;
  chosenClass = {};
  // paragraph;
  // bold;
  //levelProgression = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Class');
    this.unwrappedClassObj = this.navParams.data;
    //this.chosenClass = this.unwrappedClassObj;
   // this.testingText = this.unwrappedClassObj.proficiency;

    this.unwrappingObj();

  }

  unwrappingObj(){
    // var textProficiencies = this.unwrappedClassObj.proficiency;
    // // Formatacao do texto (para poder separa-lo em paragrafos)
    // this.unwrappedClassObj.proficiency = textProficiencies.split('|'); 

    var levels = this.unwrappedClassObj.progression;
    this.unwrappedClassObj.progression = levels; 
    this.chosenClass = this.unwrappedClassObj;
  }

  levelInfo(item) {
    //console.log(item);
    this.navCtrl.push(ClassLevelInfo, item);
  }

}


  // formattingText(){
  //   var textProficiencies = this.unwrappedClassObj.proficiency;
  //   // Formatacao do texto (para poder separa-lo em paragrafos)
  //   this.unwrappedClassObj.proficiency = textProficiencies.split('|'); 

  //   var levels = this.unwrappedClassObj.progression;
  //   this.unwrappedClassObj.progression = levels; 
  //   this.chosenClass = this.unwrappedClassObj;
  // //   var mystring = "crt/r2002_2";
  // //   mystring = mystring.replace('/r','/');

  // //   this.bold = this.paragraph;
  // //   var i;
  // //   // Bold comeca sem nenhum valor passado, apenas com o mesmo tamanho
  // // for (i = 0; i < this.paragraph.length; i++){
  // //   this.bold[i] = "";
  // // }

  // //   for (i = 0; i < this.paragraph.length; i++){
  // //     if (this.paragraph[i].indexOf('*') > -1){ // Significa que há * na string!
  // //         this.bold[i] = this.paragraph[i]; // Passo o que tiver '*' como bold
  // //         this.paragraph[i] = "$"; // Zero essa posicao do paragrafo
  // //         this.bold[i].replace('*', ''); // Limpo a string dos '*'
  // //         }   
  // //   }

  // }