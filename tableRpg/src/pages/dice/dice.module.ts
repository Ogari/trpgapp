import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Dice } from './dice';

@NgModule({
  declarations: [
    Dice,
  ],
  imports: [
    // IonicModule.forChild(Dice),
  ],
  exports: [
    Dice
  ]
})
export class DiceModule {}
