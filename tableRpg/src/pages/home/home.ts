import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Dice } from "../dice/dice"
import { SpellOptions } from "../spell-options/spell-options"
import { Monsters } from "../monsters/monsters"
import { Shop } from "../shop/shop"
import { PlayerGuide } from "../player-guide/player-guide"

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class Home {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Home');
  }
  
  goToDice(){
    this.navCtrl.push(Dice);
  }

  goToSpells(){
    this.navCtrl.push(SpellOptions);
  }

  goToMonsters(){
    this.navCtrl.push(Monsters);
  }

  goToShop(){
    this.navCtrl.push(Shop);
  }

  goToPlayerGuide(){
    this.navCtrl.push(PlayerGuide);
  }

}
