import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MonsterInfo } from './monster-info';

@NgModule({
  declarations: [
    MonsterInfo,
  ],
  imports: [
    // IonicModule.forChild(MonsterInfo),
  ],
  exports: [
    MonsterInfo
  ]
})
export class MonsterInfoModule {}
