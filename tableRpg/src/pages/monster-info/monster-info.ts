import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-monster-info',
  templateUrl: 'monster-info.html',
})
export class MonsterInfo {

  monsterInfo = {}; 
  //unwrappedObj : any;

  //All objects
  // mText : string;
  // mInfo : string;
  mDescription = {};
  mActions = [];
  mSpeed = {};
  mHp = {};
  mAttributes = {};
  // mImage = {};


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MonsterInfo');
    this.monsterInfo = this.navParams.data;

    this.wrapUndefinedObjs();

    console.log(this.monsterInfo);
  }

  wrapUndefinedObjs(){
   var unwrappedObj : any;
   unwrappedObj = this.navParams.data;

   this.mDescription = unwrappedObj.description;
   this.mSpeed = unwrappedObj.speed;
   this.mHp = unwrappedObj.hp;
   this.mAttributes = unwrappedObj.attributes;
  //  this.mImage = unwrappedObj.image;

   var i;
   for(i = 0; i < unwrappedObj.actions.length; i++){
     this.mActions.push(unwrappedObj.actions[i]);
    // this.mActions[i].damage = unwrappedObj.actions[i].damage;
   }

   console.log(this.mActions);

  }

}
