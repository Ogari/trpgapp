import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Monsters } from './monsters';

@NgModule({
  declarations: [
    Monsters,
  ],
  imports: [
    // IonicModule.forChild(Monsters),
  ],
  exports: [
    Monsters
  ]
})
export class MonstersModule {}
