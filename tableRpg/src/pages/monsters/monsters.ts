import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

// Banco de dados geral
import { RpgDatabaseApi } from '../../shared/rpg-database-api-service';

import { MonsterInfo } from '../monster-info/monster-info';

import * as _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-monsters',
  templateUrl: 'monsters.html',
})
export class Monsters {

  monsters = [];
  resetMonsters = []; // Aux para manter a lista inicial constante 
  // Aux para metodo de filtrar por nivel
  filters = [{name: "Name: A-Z", value: 0}, {name: "Name: Z-A", value: 1}, {name: "HP: Min-Max", value: 2},
   {name: "HP: Max-Min", value: 3}, {name: "Exp: Min-Max", value: 4}, {name: "Exp: Max-Min", value: 5}];

  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   private loadingController: LoadingController,
   private rpgDatabaseApi: RpgDatabaseApi) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Monsters');

        let loader = this.loadingController.create({
      content: 'Loading'
    });
    loader.present().then(()=>{
      this.rpgDatabaseApi.getFocusedRpgDatabase("0/properties.json").subscribe( data=> {
        this.monsters = data; // "monsters" recebe todos os monstros do banco
        
        // Ordeno alfabeticamente
        this.monsters = _.orderBy(this.monsters, ['name'], ['asc']);

        // Variavel auxiliar para resetar valores inicias (para nao perder busca) em searchbar
        this.resetMonsters = this.monsters

        loader.dismiss();
        console.log("Loaded");
      });
    });
  }


  goToMonsterInfo(item){
    this.navCtrl.push(MonsterInfo, item);
  }


  getSearchBarMonsters(ev) {
    // Reseto valores para não perder os anteriores
    this.monsters = this.resetMonsters;

    var val = ev.target.value;
    if (val && val.trim() != '') {
      this.monsters = this.monsters.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  filter(num : number){
// Reseto valores para não perder os anteriores
    this.monsters = this.resetMonsters;

  // filters = [{name: "Name: A-Z"}, {name: "Name: Z-A"}, {name: "HP: Min-Max"},
  //  {name: "HP: Max-Min"}, {name: "Exp: Min-Max"}, {name: "Exp: Max-Min"}];
    var order = this.filters[num].name;

      switch(order) { 
   case "Name: A-Z": {
     this.monsters = _.orderBy(this.monsters, ['name'], ['asc']);
      break; 
   } 
   case "Name: Z-A": {
     this.monsters = _.orderBy(this.monsters, ['name'], ['desc']);
      break; 
   }
   case "HP: Min-Max": {
     this.monsters = _.orderBy(this.monsters, ['hp.standard'], ['asc']);
      break; 
   }
   case "HP: Max-Min": {
     this.monsters = _.orderBy(this.monsters, ['hp.standard'], ['desc']);
      break; 
   }
   case "Exp: Min-Max": {
     this.monsters = _.orderBy(this.monsters, ['exp'], ['asc']);
      break; 
   }
   case "Exp: Max-Min": {
     this.monsters = _.orderBy(this.monsters, ['exp'], ['desc']);
      break; 
   }
   default: { 
      console.log("ERROR! - Unpredicted filter") 
      break; 
   } 
}

}


}
