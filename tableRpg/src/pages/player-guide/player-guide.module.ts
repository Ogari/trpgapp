import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PlayerGuide } from './player-guide';

@NgModule({
  declarations: [
    PlayerGuide,
  ],
  imports: [
    // IonicModule.forChild(PlayerGuide),
  ],
  exports: [
    PlayerGuide
  ]
})
export class PlayerGuideModule {}
