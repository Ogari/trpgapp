import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { RpgDatabaseApi } from '../../shared/rpg-database-api-service';
import * as _ from 'lodash';

import { Class } from "../class/class"
import { Race } from "../race/race"

@IonicPage()
@Component({
  selector: 'page-player-guide',
  templateUrl: 'player-guide.html',
})
export class PlayerGuide {

  tab: string = "race";

  allPlayerInfo : any;
  classes = [];
  races = [];

  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   private loadingController: LoadingController,
   private rpgDatabaseApi: RpgDatabaseApi) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayerGuide');

    let loader = this.loadingController.create({
      content: 'Loading'
    });
    loader.present().then(()=>{
      this.rpgDatabaseApi.getFocusedRpgDatabase("3/properties.json").subscribe( data=> {
        this.allPlayerInfo = data; // "spells" recebe todos os feiticos

        // Uma variavel para cada aba
        this.classes = this.allPlayerInfo[0].class; // Aba de 'class'
        this.races = this.allPlayerInfo[1].race; // Aba de 'race'

        // Ordeno ambos alfabeticamente
        this.classes = _.orderBy(this.classes, ['name'], ['asc']);
        this.races = _.orderBy(this.races, ['name'], ['asc']);

        loader.dismiss();
        console.log("Loaded");
      });
    });

  }

    classItemTapped(item) {
    //console.log(item);
    this.navCtrl.push(Class, item);
  }

    raceItemTapped(item) {
    //console.log(item);
    this.navCtrl.push(Race, item);
  }

}
