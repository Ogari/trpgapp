import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Race } from './race';

@NgModule({
  declarations: [
    Race,
  ],
  imports: [
    // IonicModule.forChild(Race),
  ],
  exports: [
    Race
  ]
})
export class RaceModule {}
