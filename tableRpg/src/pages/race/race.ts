import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Race page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-race',
  templateUrl: 'race.html',
})
export class Race {

  wrappedClassObj : any;
  chosenRace = {};
  bonuses = {};
  //teste = "<h2>TESTANDO</h2>";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Race');
    this.wrappedClassObj = this.navParams.data;

    this.unwrappingObj();
  }

  unwrappingObj(){
    var text = this.wrappedClassObj.generalInfo;
    // Formatacao do texto (para poder separa-lo em paragrafos)
    this.wrappedClassObj.generalInfo = text.split('|'); 

    // Unwrap do objeto 'bonus'
    this.bonuses = this.wrappedClassObj.bonus;

    // Unwrap do vetor de 'subrace'
    var subRaces = [];
    subRaces = this.wrappedClassObj.subRace;    

    this.chosenRace = this.wrappedClassObj;
  }

}
