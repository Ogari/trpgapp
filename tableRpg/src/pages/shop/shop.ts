import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { RpgDatabaseApi } from '../../shared/rpg-database-api-service';
import * as _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-shop',
  templateUrl: 'shop.html',
})
export class Shop {

  marketItems: string = "armors";

  allShopItems: any;
  // Uma variavel para cada 
  armorItems = [];
  weaponItems = [];
  tradeGoodItems = [];

  resetArmor = [];
  resetWeapon = [];
  resetTradeGoods = [];

  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   private loadingController: LoadingController,
   private rpgDatabaseApi: RpgDatabaseApi) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Shop');

        let loader = this.loadingController.create({
      content: 'Loading'
    });
    loader.present().then(()=>{
      this.rpgDatabaseApi.getFocusedRpgDatabase("2/properties.json").subscribe( data=> {
        this.allShopItems = data; // "spells" recebe todos os feiticos

        // Uma variavel para cada aba
        this.weaponItems = this.allShopItems[0].weapons;
        this.armorItems = this.allShopItems[1].armor;
        this.tradeGoodItems = this.allShopItems[2].tradeGoods;

        // Converto os valores fixados (como se fossem 'ENUM's)
        this.translateEnums();

        // Ordeno tudo alfabeticamente
        this.weaponItems = _.orderBy(this.weaponItems, ['name'], ['asc']);
        this.armorItems = _.orderBy(this.armorItems, ['name'], ['asc']);
        this.tradeGoodItems = _.orderBy(this.tradeGoodItems, ['name'], ['asc']);

        // Passo os valores para manter um ponteiro em caso de 'reset'
        this.resetArmor = this.armorItems;
        this.resetWeapon = this.weaponItems;
        this.resetTradeGoods = this.tradeGoodItems;


        //this.filter = this.navParams.data; // "filter" recebe os parametros de filtro

        // Variavel auxiliar para resetar valores inicias (para nao perder busca) em searchbar
       // this.resetFilteredSpells = this.filteredSpells;

        loader.dismiss();
        console.log("Loaded");
      });
    });
  }

getSearchBarItem(ev) {
    // Reseto valores. Necessario! Do contrario, se excluo alguns valores, nunca mais os tenho de volta
        this.armorItems = this.resetArmor;
        this.weaponItems = this.resetWeapon;
        this.tradeGoodItems = this.resetTradeGoods;

    // Valor do evento recebido (tecla digitada)
    var val = ev.target.value;
    // Filtrar para searchbar com strings nao vazias
    if (val && val.trim() != '') {
      this.armorItems = this.armorItems.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      this.weaponItems = this.weaponItems.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      this.tradeGoodItems = this.tradeGoodItems.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  translateEnums(){
    var i;
    var currency;
    var armorType;

    // Translate Enums (WEAPONS)
    for(i = 0; i < this.weaponItems.length; i++){
      // Convert currency values (WEAPONS)
      currency = this.weaponItems[i].cost.currency;
      if(currency == 0){    this.weaponItems[i].cost.currency = "cp";    }
      if(currency == 1){    this.weaponItems[i].cost.currency = "sp";    }
      if(currency == 2){    this.weaponItems[i].cost.currency = "gp";    }
      
      else{ console.log("ERROR! - (WEAPON Enum error) Unpredicted currency value")}
    }

    // Translate Enums (ARMORS)
    for(i = 0; i < this.armorItems.length; i++){

      // Convert currency values (ARMORS)
      currency = this.armorItems[i].cost.currency;
      if(currency == 0){    this.armorItems[i].cost.currency = "cp";    }
      if(currency == 1){    this.armorItems[i].cost.currency = "sp";    }
      if(currency == 2){    this.armorItems[i].cost.currency = "gp";    }

      // Convert armor types (ARMORS)
      armorType = this.armorItems[i].armorType;
      if(armorType == 0){    this.armorItems[i].armorType = "Light";    }
      if(armorType == 1){    this.armorItems[i].armorType = "Medium";   }
      if(armorType == 2){    this.armorItems[i].armorType = "Heavy";    }
      if(armorType == 3){    this.armorItems[i].armorType = "Shield";   }
      
      else{ console.log("ERROR! - (ARMOR Enum error) Unpredicted currency value or armor type")}
    }

    // Translate Enums (TRADE GOODS)
    for(i = 0; i < this.tradeGoodItems.length; i++){
      // Convert currency values (TRADE GOODS)
      currency = this.tradeGoodItems[i].cost.currency;
      if(currency == 0){    this.tradeGoodItems[i].cost.currency = "cp";    }
      if(currency == 1){    this.tradeGoodItems[i].cost.currency = "sp";    }
      if(currency == 2){    this.tradeGoodItems[i].cost.currency = "gp";    }
      
      else{ console.log("ERROR! - (TRADE GOODS Enum error) Unpredicted currency value")}
    }



  }

}
