import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { SpellInfo } from './spell-info';

@NgModule({
  declarations: [
    SpellInfo,
  ],
  imports: [
    // IonicModule.forChild(SpellInfo),
  ],
  exports: [
    SpellInfo
  ]
})
export class SpellInfoModule {}
