import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

/**
 * Generated class for the SpellInfo page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-spell-info',
  templateUrl: 'spell-info.html',
})
export class SpellInfo {

// NAO ESQUECER DE INICIALIZAR COMO OBJETO (do contrario, eh uma variavel indefinida)
  spellInfo = {}; 
  schoolName : String;
  spellcastersName = [];
  //spellcasterString : String;

  constructor(public navCtrl: NavController, 
  public navParams: NavParams, 
  private loadingController : LoadingController) {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpellInfo');
    this.spellInfo = this.navParams.data;

    this.namingCases();
    console.log(this.spellInfo);
    console.log("spellcastersName: " + this.spellcastersName);
  }


  namingCases(){
// <!--0 = abjuration // 1 = conjuration// 2 = divination // 3 = enchantment
// 4 = evocation// 5 = illusion// 6 = necromancy// 7 = trasmutation-->
var spell = this.navParams.data;
// Substituo por nome a escola de feitico
    switch(spell.school) { 
   case 0: { this.schoolName =  "Abjuration";
      break; 
   } 
   case 1: {this.schoolName =  "Conjuration";
      break; 
   }
   case 2: {this.schoolName =  "Divination";
      break; 
   }
   case 3: {this.schoolName =  "Enchantment";
      break; 
   }
   case 4: {this.schoolName =  "Evocation";
      break; 
   }
   case 5: {this.schoolName =  "Illusion";
      break; 
   }
   case 6: {this.schoolName =  "Necromancy";
      break; 
   }
   case 7: {this.schoolName =  "Trasmutation";
      break; 
   } 
   default: { 
      console.log("ERROR! - School not currently available") 
      break; 
   } 
}
// <!--0 = bard// 1 = cleric// 2 = druid// 3 = paladin// 4 = ranger
// 5 = sorcerer// 6 = warlock// 7 = wizard-->
var i;
// Substituo por um vetor de nomes pra todos os potenciais "spellcasters"
for (i = 0; i < spell.magicUser.length; i++){
      switch(spell.magicUser[i]) { 
   case 0: {
     //this.spellcastersName.concat(" Bard,");
     spell.magicUser[i] = " Bard";
      break; 
   } 
   case 1: {
    spell.magicUser[i] = " Cleric";
      break; 
   }
   case 2: {
     spell.magicUser[i] = " Druid";
      break;
   }
   case 3: { 
     spell.magicUser[i] = " Paladin";
      break;
   }
   case 4: { 
     spell.magicUser[i] = " Ranger";
      break; 
   }
   case 5: { 
     spell.magicUser[i] = " Sorcerer";
      break; 
   }
   case 6: { 
     spell.magicUser[i] = " Warlock";
      break; 
   }
   case 7: { 
     spell.magicUser[i] = " Wizard";
      break; 
   } 
   default: { 
      console.log("ERROR! - Spellcaster not currently available") 
      break; 
   } 
}
}

// NECESSARIO!! Soh podem ser passados objetos para serem lidos
this.spellcastersName = spell.magicUser;

  }

}
