import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { SpellOptions } from './spell-options';

@NgModule({
  declarations: [
    SpellOptions,
  ],
  imports: [
    // IonicModule.forChild(SpellOptions),
  ],
  exports: [
    SpellOptions
  ]
})
export class SpellOptionsModule {}
