import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Spells } from "../spells/spells"

/**
 * Generated class for the SpellOptions page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-spell-options',
  templateUrl: 'spell-options.html',
})
export class SpellOptions {
  
  magicSchools = [{name: "Abjuration", bool: false}, {name: "Conjuration", bool: false},
  {name: "Divination", bool: false}, {name: "Enchantment", bool: false},
  {name: "Evocation", bool: false}, {name: "Illusion", bool: false},
  {name: "Necromancy", bool: false}, {name: "Trasmutation", bool: false} 
];

  spellcasters = [{name: "Bard", bool: false}, {name: "Cleric", bool: false},
  {name: "Druid", bool: false}, {name: "Paladin", bool: false},
  {name: "Ranger", bool: false}, {name: "Sorcerer", bool: false},
  {name: "Warlock", bool: false}, {name: "Wizard", bool: false} 
];

  paramPass = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpellOptions');
  }

  goToFilteredSpells(){
    this.paramPass = [this.magicSchools, this.spellcasters];
    this.navCtrl.push(Spells, this.paramPass);
  }

  toggleMagicSchools(i){
    if(this.magicSchools[i].bool == true){
      this.magicSchools[i].bool = false;
    console.log("Recebeu FALSE - MAGIC SCHOOL");
  }
  else{
    this.magicSchools[i].bool = true;
    console.log("Recebeu TRUE - MAGIC SCHOOL");
  }

}

toggleSpellcasters(i){
    if(this.spellcasters[i].bool == true){
      this.spellcasters[i].bool = false;
    console.log("Recebeu FALSE - SPELLCASTER");
  }
  else{
    this.spellcasters[i].bool = true;
    console.log("Recebeu TRUE - SPELLCASTER");
  }

  }


}
