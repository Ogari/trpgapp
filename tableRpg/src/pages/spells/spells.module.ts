import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Spells } from './spells';

@NgModule({
  declarations: [
    Spells,
  ],
  imports: [
    // IonicModule.forChild(Spells),
  ],
  exports: [
    Spells
  ]
})
export class SpellsModule {}
