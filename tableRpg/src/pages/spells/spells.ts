import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

// Banco de dados geral
import { RpgDatabaseApi } from '../../shared/rpg-database-api-service';

// Ir pra pagina em questao
import { SpellInfo } from '../spell-info/spell-info';

import * as _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-spells',
  templateUrl: 'spells.html',
})
export class Spells {

   spells = [];
   filter : any;
   arrayMagicSchools = [];
   arraySellcasters = [];
   filteredSpells = [];

   // Aux para funcao da searchbar
   resetFilteredSpells = [];

   // Aux para metodo de filtrar por nivel
   levels = [{name: "All", value: -1}, {name: "0", value: 0}, {name: "1", value: 1}, {name: "2", value: 2},
   {name: "3", value: 3}, {name: "4", value: 4}, {name: "5", value: 5}];
   level = -1;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private rpgDatabaseApi: RpgDatabaseApi,
              private loadingController: LoadingController ) {

                //this.filter = this.navParams.data;
              }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Spells');

    let loader = this.loadingController.create({
      content: 'Loading'
    });
    loader.present().then(()=>{
      this.rpgDatabaseApi.getFocusedRpgDatabase("1/properties.json").subscribe( data=> {
        this.spells = data; // "spells" recebe todos os feiticos
        this.filter = this.navParams.data; // "filter" recebe os parametros de filtro

        //Crio os arrays auxiliares
        this.arrayConversion();

        //Repasso todos os feiticos filtrados a "filteredSpells"
        this.filterSpells();

        // Variavel auxiliar para resetar valores inicias (para nao perder busca) em searchbar
        this.resetFilteredSpells = this.filteredSpells;

        loader.dismiss();
        console.log("Loaded");
      });
    });
  }
  
  itemTapped(item) {
    console.log(item);
    this.navCtrl.push(SpellInfo, item);
  }


// Monto 2 arrays. Um com todas as escolas de magia e outro com todos que podem usar as magias
arrayConversion(){
  var i;
  // Faco o array de feiticos buscados
  for (i = 0; i < this.filter[0].length; i++){
    if(this.filter[0][i].bool == true){
      this.arrayMagicSchools.push(i);
    }
  }
    for (i = 0; i < this.filter[1].length; i++){
      if(this.filter[1][i].bool == true){
        this.arraySellcasters.push(i);
    }
  }

}

// Faco um filtro com os arrays que criei, comparando se os valores dentro deles batem com quais feiticos
filterSpells(){
  var i;
  // Pegar todas as possibilidades para "magicSchools" 
  for (i = 0; i < this.arrayMagicSchools.length; i++){
    var aux = _.filter(this.spells, { 'school': this.arrayMagicSchools[i] });
    this.filteredSpells = _.union(this.filteredSpells, aux);
  }
// Pegar todas as possibilidades para "spellcasters" 
    for (i = 0; i < this.arraySellcasters.length; i++){
    var aux = _.filter(this.spells, { 'magicUser': [ this.arraySellcasters[i] ] });
    this.filteredSpells = _.union(this.filteredSpells, aux);
  }
  // Ordeno em ordem alfabetica
  this.filteredSpells = _.orderBy(this.filteredSpells, ['name'], ['asc']);
}

// Metodo da searchbar
  getSearchBarSpells(ev) {
    // Reseto valores. Necessario! Do contrario, se excluo alguns valores, nunca mais os tenho de volta
    this.filteredSpells = this.resetFilteredSpells;

    // Valor do evento recebido (tecla digitada)
    var val = ev.target.value;
    // Filtrar para searchbar com strings nao vazias
    if (val && val.trim() != '') {
      this.filteredSpells = this.filteredSpells.filter((item) => {
        // Passo os valores 
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }


filterByLevel(num : number){
// Reseto valores. Necessario! Do contrario, se excluo alguns valores, nunca mais os tenho de volta
    this.filteredSpells = this.resetFilteredSpells;

    var val = this.levels[num].value;

  if (val != -1){
  this.filteredSpells = _.chain(this.filteredSpells)
                        .filter(item => item.level == val)
                        .value();
  }
  console.log(this.filteredSpells);
}

}
