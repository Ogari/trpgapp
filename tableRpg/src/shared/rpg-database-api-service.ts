import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import 'rxjs';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RpgDatabaseApi {
    private baseUrl = 'https://dungeonsdragonsdata.firebaseio.com/';

    entireDatabase = [];

    constructor(private http: Http){

    }

    getFocusedRpgDatabase(object : string): Observable<any>{
        var str = this.baseUrl.concat(object);
        // return this.http.get(`${this.baseUrl}.json`)
        return this.http.get(str)
            .map( (response : Response) => {
                this.entireDatabase = response.json();
                return this.entireDatabase;
             });
    }


// ORIGINAL (FUNCIONANDO)
    //     getEntireRpgDatabase(): Observable<any>{

//      return this.http.get(`${this.baseUrl}.json`)
    //     return this.http.get(str)
    //         .map( (response : Response) => {
    //             this.entireDatabase = response.json();
    //             return this.entireDatabase;
    //          });
    // }
    
}